<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['preventbackhistory']], function () {

    Route::group(['middleware' => ['checkislogin']], function () {
        Route::get('/', 'LoginController@index');
        Route::get('/login', 'LoginController@index');
    });
    
    Route::group(['middleware' => ['checkisuser']], function () {
        Route::get('/main', 'MainController@index');
    });

});
